<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'macankumbang' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'iFbBpUcywDbpvJnG-R=XUlAd25}&:E`]{2SUzYrTUd40?:3V|bnq+O?t[!,]Vzwf' );
define( 'SECURE_AUTH_KEY',  '=66-}J.<2<Y-!&blj-m1|[2IO^A{KIl)KWM[LeDGoGEm#?!Zd=%3vE9K:z&Ik2sR' );
define( 'LOGGED_IN_KEY',    '4pk=JyAtNONTr%jO+2#Cq-Q<x%7z}/C;vBdvJ4y7BeQ|o9X1{B_hpt>1(D^{bgzi' );
define( 'NONCE_KEY',        '5L.Tl,[Gn(yCS9i-)=2((PT CT;KbPAe,cZzx]YV U-:W9P<6#VB53UnZggI<*^z' );
define( 'AUTH_SALT',        '6Rn*@6_4;=!iBwEGPSkQC+g#, Vt,r2q5h,rFN.6X#C.51^7[9=k(vGR)0HF]CzJ' );
define( 'SECURE_AUTH_SALT', '*W+Xw1==7t@%C#l)9%oGsj-.IS|i2tt:&v%+nEO`.]rQ0Bmx<[%.DtbZ,41u<//m' );
define( 'LOGGED_IN_SALT',   'Y+]T}W,D<M<vU--ozUf*HC!.7&bn$gTp.i,FBt2NLFjMLxt#ZIAcr/;?myIl(zec' );
define( 'NONCE_SALT',       'NM<k9>_;SS*2,MZEvI~>d6{+6nnhXK/}XOtAnRZ-j[:HFE,r&!IuFew[4_qyS2NF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'macankumbang_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
