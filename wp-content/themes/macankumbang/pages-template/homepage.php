<?php /* Template Name: Homepage */ ?>
<?php get_header() ?>
<?php get_template_part('template-parts/homepage/services') ?>
<?php get_template_part('template-parts/homepage/works') ?>
<?php get_template_part('template-parts/homepage/portofolio') ?>
<?php get_template_part('template-parts/homepage/testimonials') ?>
<!-- <?php get_template_part('template-parts/homepage/touch') ?> -->
<?php get_template_part('template-parts/homepage/contact') ?>
<?php get_footer() ?>