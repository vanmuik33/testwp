<?php /* Template Name: Coupon */ ?>
<?php get_header() ?>
<h1>Here is coupon-template.php</h1>
<?php
//Khai báo tên post type sẽ được hiển thị và số bài hiển thị mỗi trang
$args = array('post_type' => 'coupon_code', 'posts_per_page' => 10);
$loop = new WP_Query($args);
while ($loop->have_posts()) : $loop->the_post();
?>
    <header class="entry-header">
        <h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" class="entry-title"><?php the_title(); ?></a></h1>
    </header>
<?php
    echo '<div class="entry-content">';
    the_post_thumbnail();
    the_content();
    echo '</div>';

endwhile;
?>
<?php get_footer() ?>