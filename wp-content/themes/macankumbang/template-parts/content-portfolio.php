<div class="element-item col-xs-12 col-sm-6 col-md-4 <?php echo get_categories_list() ?>">
    <div class="img-item">
        <!-- <div style="height: 270px; width: 360px; overflow: hidden;"><img class="img-responsive hori-center"  src="<?php echo get_the_post_thumbnail_url() ?>" alt="portofolio"></div> -->
        <?php the_post_thumbnail( 'custom-size' ) ?>
        <div class="over-lay">
            <div class="content">
                <h1><?php the_title() ?></h1>
                <?php the_content() ?>
                <span><i class="icon-eye"></i></span>
            </div>
        </div>
    </div>
</div>