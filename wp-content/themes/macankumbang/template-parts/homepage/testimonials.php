<section id="testimonials">
    <div class="container">
        <div class="slick-slider">
            <div class="web">
                <h1 class="small-title">What Our Happy Customer Say</h1>
                <p class="small-desc">lorem ipsum dolor sit amet</p>
                <img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/port.png" alt="">
                <div class="row customer">
                    <div class="col-xs-12 col-md-4 col-md-offset-0">
                        <div class="avatar hori-center"
                            style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/testimonials/c1.jpg);">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 col-md-offset-0 content">
                        <div class="left">
                            <img src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/dots.png" alt="dot">
                        </div>
                        <div class="right">
                            <p class="said">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                                auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat
                                ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
                                amet mauris. </p>
                            <p class="author">John Doe</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="web">
                <h1 class="small-title">What Our Happy Customer Say</h1>
                <p class="small-desc">lorem ipsum dolor sit amet</p>
                <img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/port.png" alt="">
                <div class="row customer">
                    <div class="col-xs-12 col-md-4 col-md-offset-0">
                        <div class="avatar hori-center"
                            style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/testimonials/c2.jpg);">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 col-md-offset-0 content">
                        <div class="left">
                            <img src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/dots.png" alt="dot">
                        </div>
                        <div class="right">
                            <p class="said">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                                auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat
                                ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
                                amet mauris. </p>
                            <p class="author">John Doe</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <h1 class="small-title">What Our Happy Customer Say</h1>
                <p class="small-desc">lorem ipsum dolor sit amet</p>
                <img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/port.png" alt="">
                <div class="row customer">
                    <div class="col-xs-12 col-md-4 col-md-offset-0">
                        <div class="avatar hori-center"
                            style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/testimonials/c3.jpg);">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 col-md-offset-0 content">
                        <div class="left">
                            <img src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/dots.png" alt="dot">
                        </div>
                        <div class="right">
                            <p class="said">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                                auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat
                                ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
                                amet mauris. </p>
                            <p class="author">John Doe</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <h1 class="small-title">What Our Happy Customer Say</h1>
                <p class="small-desc">lorem ipsum dolor sit amet</p>
                <img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/port.png" alt="">
                <div class="row customer">
                    <div class="col-xs-12 col-md-4 col-md-offset-0">
                        <div class="avatar hori-center"
                            style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/testimonials/c4.jpg);">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 col-md-offset-0 content">
                        <div class="left">
                            <img src="<?php echo TPL_DIR_URI ?>/assets/images/testimonials/dots.png" alt="dot">
                        </div>
                        <div class="right">
                            <p class="said">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                                auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat
                                ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
                                amet mauris. </p>
                            <p class="author">John Doe</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>