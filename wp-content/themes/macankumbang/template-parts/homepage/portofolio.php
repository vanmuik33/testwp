<section id="portofolio">
    <div class="container">
        <h1 class="small-title">Portofolio</h1>
        <p class="small-desc">our awesome works</p>
        <img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/portofolio/port.png" alt="">
    </div>
    <ol class="breadcrumb hori-center">
        <li class="active" data-filter="*">All</li>
        <?php
        $args = array(
            'type'                     => 'portofolio',
            'taxonomy'                 => 'tut'
        );
        $categories = get_categories($args);
        foreach ($categories as $category) {
            $url = get_term_link($category); ?>
            <li class="" data-filter=".<?php echo $category->category_nicename; ?>"><?php echo $category->name; ?></li>
        <?php
        }
        ?>
    </ol>
    <div class="container">
        <div class="row ">
            <div class="grid">
                <!-- <div class="imglist"> -->
                <?php
                $args = array(
                    'post_type' => 'portofolio',
                    'post_status' => 'publish',
                    'posts_per_page' => 3,
                    'paged' => get_query_var('paged')
                );
                $list_portofolio = new WP_Query($args);
                if ($list_portofolio->have_posts()) :
                    while ($list_portofolio->have_posts()) : $list_portofolio->the_post();
                        get_template_part('template-parts/content', 'portfolio');
                    endwhile;
                endif;
                // Reset Query
                wp_reset_postdata();
                ?>
                <!-- </div> -->
            </div>
        </div>
    </div>
    <div class="browse text-center">
        <p>Browse All</p>
        <p>300+</p>
        <img src="<?php echo TPL_DIR_URI ?>/assets/images/portofolio/dwn-arrow.png" id="browse" alt="Browse All">
    </div>
    <div class="display-post">
    </div>
</section>