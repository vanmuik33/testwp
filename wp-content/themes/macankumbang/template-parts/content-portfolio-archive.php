<div class="col-sm-4">
    <a href="<?php the_permalink() ?>" class="thumbnail">
        <h3><?php the_title() ?></h3>
        <img class="portfolio-thumbnail" src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
        <?php the_excerpt() ?>
    </a>
</div>





<!-- <a href="<?php the_permalink() ?>" class="thumbnail">
    <h3><?php the_title() ?></h3>
    <img class="portfolio-thumbnail" src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
    <?php the_excerpt() ?>
</a> -->