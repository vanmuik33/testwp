<?php
add_action('wp_ajax_getarchive', 'get_archive_function');
add_action('wp_ajax_nopriv_getarchive', 'get_archive_function');
function get_archive_function()
{
    $current_page = $_GET['paged'];
    $order = $_GET['order'];
    $term_id = empty($_GET['filter']) ? '' : $_GET['filter'];

    // $taxonomies = get_terms(array(
    //     'taxonomy' => 'tut',
    //     'hide_empty' => false
    // ));

    // foreach ($taxonomies as $taxonomy) {
    //     $term_id[] = $taxonomy->slug;
    // }
    $args = array(
        'post_type' => 'portofolio',
        'post_status' => 'publish',
        'paged' => $current_page,
        'posts_per_page' => $_GET['post_per_page'],
        'order' => empty($_GET['order']) ? null : $_GET['order'],
    );

    if (!empty($term_id)){
       
        $args['tax_query'] = array(                     //(array) - Lấy bài viết dựa theo taxonomy
            array(
                'taxonomy' => 'tut',                //(string) - Tên của taxonomy
                'field' => 'slug',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
                'terms' => $term_id,    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
                'include_children' => true,           //(bool) - Lấy category con, true hoặc false
                'operator' => 'IN'                    //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
            ),
        );
    }
    
    $list_portofolio = new WP_Query($args);
    $max_num_pages = $list_portofolio->max_num_pages;
    $pagination = '';
    if ($list_portofolio->have_posts()) {
        if ($max_num_pages > 1) {
            $pagination .= '<ul class="pagination bounceInUp animated wow" data-wow-delay=".8s">';
            for ($i = 1; $i <= $max_num_pages; $i++) {
                if ($i == $current_page) {
                    $pagination .= '<li><span aria-current="page" class="page-numbers current">' . $current_page . '</span></li>';
                } else {
                    $pagination .= '<li><a class="page-numbers" data-page="' . $i . '" data-order="'.$order.'" data-filter="'.$_GET['filter'].'" href="#">' . $i . '</a></li>';
                }
            }
            $pagination .= '</ul>';
        }
        ob_start();
        while ($list_portofolio->have_posts()) {
            $list_portofolio->the_post();
            get_template_part('template-parts/content', 'portfolio-archive');
        }
    }
    wp_reset_postdata();

    $html = ob_get_contents();
    $response = array(
        'message' => 'success',
        'html' => $html,
        'pagination' => $pagination
    );
    ob_end_clean();
    wp_send_json_success($response, 200);
    die();
}
