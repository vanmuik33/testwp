<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makacumbang
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD79wZYkkpRfxcnqxDeWXxz-6jKf9S1WS4"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<section id="header">
			<div id="navbar">
				<div class="container">
					<div class="topnav clearfix">
						<div class="logo">
							<a href="<?php echo get_option("siteurl"); ?>"><img src="<?php echo TPL_DIR_URI ?>'/assets/images/header/logo.png'" alt="Macankumbang"></a>
							<a href="javascript:void(0);" class="icon" onclick="myFunction()">
								<i class="icon-menu"></i>
							</a>
						</div>
						<nav class="navbar" id="myNavbar">
							<a class="active" href="<?php echo get_option("siteurl"); ?>">Home</a>
							<a href="#services">Services</a>
							<a href="#works">Work</a>
							<a href="#testimonials">Testimonials</a>
							<a href="#contacts">Contact</a>
						</nav>
						<!-- <?php display_menu('main-menu'); ?> -->
					</div>
				</div>
			</div>
			<div id="header-content" style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/header/desk.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-7 Box">
							<h1 class="title header-title">
								Kalau Kita mau, Pasti ada Jalan
							</h1>
							<p class="desc">Semangat terus saudara-saudara!</p>
							<button class="pri-btn">More</button>
						</div>
						<div class="col-xs-12 col-md-5 ">

							<div class="intro-img" style="background-image: url(<?php echo TPL_DIR_URI ?>/assets/images/header/desk.jpg);">

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>