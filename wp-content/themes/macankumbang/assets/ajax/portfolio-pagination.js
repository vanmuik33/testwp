jQuery(document).ready(function ($) {

  $("#pagination").on("click",".page-numbers", function (e) {
    e.preventDefault();
    // AJAX
    $page = $(this).attr("data-page");
    $order = $(this).attr("data-order");
    $filter = $(this).attr("data-filter");
    $.ajax({
      url: my_ajaxurl,
      type: "get",
      dataType: "json",
      data: {
        action: "get_pagination",
        posts_per_archive_page: 6,
        page: $page,
        order: $order,
        filter: $filter,
      },
      success: function (ketqua) {
        // Thử nghiệm
        $("#pagination").html(ketqua['data']['pagination']);
        $('#archive-content').html(ketqua['data']['html']);
      },
    });
  });
});
