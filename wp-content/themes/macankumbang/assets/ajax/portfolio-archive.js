jQuery(document).ready(function () {
  jQuery(".dropdown-item").on("click", function () {
    var order = jQuery(this).attr("data-order");
    var last_filter = jQuery('#last_filter').val();
    var page = 1;
    console.log(order);
    jQuery.ajax({
      // Hàm ajax
      type: "get", //Phương thức truyền post hoặc get
      dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
      url: my_ajaxurl, // Nơi xử lý dữ liệu
      data: {
        action: "getarchive", //Tên action, dữ liệu gởi lên cho server
        post_per_page: 6,
        order: order,
        filter: last_filter,
        paged: page,
      },
      beforeSend: function () {
        // Có thể thực hiện công việc load hình ảnh quay quay trước khi đổ dữ liệu ra
      },
      success: function (response) {
        jQuery('#last_order').val(order);
        jQuery('#archive-content').html(response['data']['html']);
        jQuery('.page-numbers').attr({'data-order' : order,'data-filter':last_filter});
        jQuery("#pagination").html(response['data']['pagination']);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        //Làm gì đó khi có lỗi xảy ra
        console.log("The following error occured: " + textStatus, errorThrown);
      },
    });
  });
  jQuery(".filter-button").on("click", function () {
    var filter = jQuery(this).attr("data-filter");
    var last_order = jQuery('#last_order').val();
    var page = jQuery('#paged').val();
    // console.log(filter);
    jQuery.ajax({
      // Hàm ajax
      type: "get", //Phương thức truyền post hoặc get
      dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
      url: my_ajaxurl, // Nơi xử lý dữ liệu
      data: {
        action: "getarchive", //Tên action, dữ liệu gởi lên cho server
        post_per_page: 6,
        filter: filter,
        order: last_order,
        paged: page
      },
      beforeSend: function () {
        // Có thể thực hiện công việc load hình ảnh quay quay trước khi đổ dữ liệu ra
      },
      success: function (response) {
        console.log(response);
        jQuery('#last_filter').val(filter);
        jQuery('#archive-content').html(response['data']['html']);
        jQuery('.page-numbers').attr({'data-order' : last_order,'data-filter':filter});
        jQuery("#pagination").html(response['data']['pagination']);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        //Làm gì đó khi có lỗi xảy ra
        console.log("The following error occured: " + textStatus, errorThrown);
      },
    });
  });
});
