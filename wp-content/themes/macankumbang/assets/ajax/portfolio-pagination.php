<?php
add_action('wp_ajax_get_pagination', 'get_pagination_function');
add_action('wp_ajax_nopriv_get_pagination', 'get_pagination_function');
function get_pagination_function()
{
    $taxonomies = get_terms(array(
        'taxonomy' => 'tut',
        'hide_empty' => false
    ));

    foreach ($taxonomies as $taxonomy) {
        $term_id[] = $taxonomy->slug;
    }

    $pagination = '';
    if (isset($_GET['page'])) {
        $current_page = $_GET['page'];
        $posts_per_archive_page = $_GET['posts_per_archive_page'];
        $order = empty($_GET['order']) ? null : $_GET['order'];
        $filter = empty($_GET['filter']) ? $term_id : array($_GET['filter']);
        $args = array(
            'post_type' => 'portofolio',
            'paged' => $current_page,
            'posts_per_archive_page' => $posts_per_archive_page,
            'post_status' => 'publish',
            'order' => $order,
            'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
                array(
                    'taxonomy' => 'tut',                //(string) - Tên của taxonomy
                    'field' => 'slug',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
                    'terms' => $filter,    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
                    'include_children' => true,           //(bool) - Lấy category con, true hoặc false
                    'operator' => 'IN'                    //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
                ),
            ),
        );

        $loop = new WP_Query($args);
        $max_num_pages = $loop->max_num_pages;
        $html = '';
        if ($loop->have_posts()) {
            if ($max_num_pages > 1) {
                $pagination .= '<ul class="pagination bounceInUp animated wow" data-wow-delay=".8s">';
                for ($i = 1; $i <= $max_num_pages; $i++) {
                    if ($i == $current_page) {
                        $pagination .= '<li><span aria-current="page" class="page-numbers current">' . $current_page . '</span></li>';
                    } else {
                        $pagination .= '<li><a class="page-numbers" data-page="' . $i . '" data-order="'.$order.'" data-filter="'.$_GET['filter'].'" href="#">' . $i . '</a></li>';
                    }
                }
                $pagination .= '</ul>';
            }
            // $pagination = $max_num_pages;
            ob_start();
            while ($loop->have_posts()) {
                $loop->the_post();
                get_template_part('template-parts/content', 'portfolio-archive');
            }
            $html = ob_get_contents();
            ob_end_clean();
        }
        $response = array(
            'message' => 'success',
            'html' => $html,
            'pagination' => $pagination
        );
        wp_send_json_success($response, 200);
    }
    exit();
}
