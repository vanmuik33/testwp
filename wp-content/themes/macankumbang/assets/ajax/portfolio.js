// jQuery(document).ready(function () {
//   jQuery("#browse").on("click", function () {
//     // Khi click vào button thì sẽ gọi hàm ajax

//     jQuery.ajax({
//       // Hàm ajax
//       type: "post", //Phương thức truyền post hoặc get
//       dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
//       url: my_ajaxurl, // Nơi xử lý dữ liệu
//       data: {
//         action: "browse_all", //Tên action, dữ liệu gởi lên cho server
//       },
//       beforeSend: function () {
//         // Có thể thực hiện công việc load hình ảnh quay quay trước khi đổ dữ liệu ra
//       },
//       success: function (response) {
//         //Làm gì đó khi dữ liệu đã được xử lý
//         // result = response.data["html"];
//         $result = response.data["listHTML"];
//         // console.log(result);
//         elements = jQuery('.web');

//         // jQuery('.grid').isotope( 'hideItemElements', elements );
//         // jQuery('.grid').isotope( 'appended', $result );
//         jQuery('.grid').isotope()
//         .append($result)
//         .isotope('appended',$result);
//       },
//       error: function (jqXHR, textStatus, errorThrown) {
//         //Làm gì đó khi có lỗi xảy ra
//         // console.log('The following error occured: ' + textStatus, errorThrown);
//         console.log("The following error occured: ");
//       },
//     });
//   });
// });

jQuery(document).ready(function () {
  var currentPage = 1; // khái báo số lượng bài viết đã hiển thị
  jQuery("#browse").on("click", function () {
    jQuery.ajax({
      // Hàm ajax
      type: "get", //Phương thức truyền post hoặc get
      dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
      url: my_ajaxurl, // Nơi xử lý dữ liệu
      data: {
        action: "browseall", //Tên action, dữ liệu gởi lên cho server
        currentPage: currentPage,
        post_per_page: 3,
      },
      beforeSend: function () {
        // Có thể thực hiện công việc load hình ảnh quay quay trước khi đổ dữ liệu ra
      },
      success: function (response) {
        console.log(response);
        var $elem = jQuery(response['data']['html']);
        jQuery(".grid").append($elem).isotope("appended", $elem);
        currentPage++;
        if (response['data']['next']==null) {
          jQuery('.browse').hide();
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        //Làm gì đó khi có lỗi xảy ra
        console.log("The following error occured: " + textStatus, errorThrown);
      },
    });
  });
});
