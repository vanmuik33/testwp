<?php
/**
 * Functions ajax portfolio
 */
add_action('wp_ajax_browseall', 'browse_all_function');
add_action('wp_ajax_nopriv_browseall', 'browse_all_function');
function browse_all_function()
{
	$args = array(
		'post_type' => 'portofolio',
		'post_status' => 'publish',
		'posts_per_page' => $_GET['post_per_page'],
		'paged' => $_GET['currentPage'] + 1
	);
	$list_portofolio = new WP_Query($args);
	ob_start();
	if ($list_portofolio->have_posts()) {
		$next = 0;
		while ($list_portofolio->have_posts()) {
			$list_portofolio->the_post();
			get_template_part('template-parts/content', 'portfolio');
			$next++;
		}
		if ($next < $_GET['post_per_page']) $next = null;
	}
	$html = ob_get_contents();
	$response = array(
		'message' => 'success',
		'html' => $html,
		'next' => $next
	);
	ob_end_clean();
	wp_send_json_success($response, 200);
	wp_reset_postdata();
	die();
}
