// ======>>>> set variable

var primaryColor2 = "#ea5b58";
var primaryColor1 = "#323232";
// =================>> navbar responsive

function myFunction() {
  jQuery("#myNavbar").toggleClass("responsive");
  jQuery("#navbar").toggleClass("h-auto");
}

// =================>> Filter
var jQuerygrid = jQuery(".grid").isotope({
  itemSelector: ".element-item",
  // layoutMode: "fitRows",
});

jQuery(document).ready(function () {
  jQuerygrid.isotope({ filter: "*" });
});

jQuery(".breadcrumb li").click(function () {
  jQuery("li.active").removeClass("active");
  jQuery(this).addClass("active");
});

// bind filter button click
jQuery(".breadcrumb").on("click", "li", function () {
  var filterValue = jQuery(this).attr("data-filter");
  // use filterFn if matches value
  jQuerygrid.isotope({ filter: filterValue });
});

// ==== portfolio ajax


// =================>>> Slick-carousel

jQuery(document).ready(function () {
  jQuery(".slick-slider").slick({
    dots: true,
    autoplay: false,
    slidesToScroll: 1,
    accessibility: false,
    arrows: false,
    swipeToSlide: true,
  });
});

/// ========================>> Portofolio arrow hover

jQuery(document).ready(function () {
  jQuery("#portofolio .browse img").hover(
    function () {
      jQuery("#portofolio .browse p").css("color", primaryColor2);
    },
    function () {
      jQuery("#portofolio .browse p").css("color", primaryColor1);
    }
  );
});

// ========================>> Easy Scroll To Top
jQuery(document).ready(function () {
  //Check to see if the window is top if not then display button
  jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 100) {
      jQuery(".easyTop").fadeIn();
    } else {
      jQuery(".easyTop").fadeOut();
    }
  });

  //Click event to scroll to top
  jQuery(".easyTop").click(function () {
    jQuery("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });
});

// =====================>> scroll spy navbar

// jQuery(document).ready(function () {
//   jQuery("nav").stickynav();
// });

// ======================>> Responsive
var windowWidth = jQuery(window).width();
if (windowWidth >= 992 && windowWidth <= 1199)
  jQuery("input[type='email']").attr("placeholder", "Email");
jQuery(window).resize(function () {
  windowWidth = jQuery(window).width();
  if (windowWidth >= 992 && windowWidth <= 1199)
    jQuery("input[type='email']").attr("placeholder", "Email");
  else {
    jQuery("input[type='email']").attr(
      "placeholder",
      "Email (will not published"
    );
  }
});

// =======================>> Google Maps
// var center = [50.211246, -5.482152];
// jQuery(".map-iframe")
//   .gmap3({
//     center: center,
//     zoom: 16,
//     mapTypeId: google.maps.MapTypeId.ROADMAP,
//   })
//   .marker({
//     position: center,
//     icon: "/wp-content/themes/macankumbang/assets/images/map/location.png",
//   });
