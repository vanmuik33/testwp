<?php 
    function html5wp_pagination($query)
    {
        $big = 999999999;
        $pages = paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $query->max_num_pages,
            'type'  => 'array',
        ));
        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<div class=""><ul class="pagination bounceInUp animated wow" data-wow-delay=".8s">';
            foreach ( $pages as $page ) {
                    echo "<li>$page</li>";
            }
           echo '</ul></div>';
            }; 
    }
