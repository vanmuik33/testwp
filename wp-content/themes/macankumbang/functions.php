<?php

/**
 * Makacumbang functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Makancumbang
 */
define('TPL_DIR_URI', get_template_directory_uri());

# Khai báo hằng số TPL_DIR bằng đường dẫn đến thư mục theme
define('TPL_DIR', get_stylesheet_directory());


if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('makacumbang_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function makacumbang_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Makacumbang, use a find and replace
		 * to change 'makacumbang' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('makacumbang', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');
		add_image_size('custom-size', 360, 224, array('left', 'top'));
		the_post_thumbnail('custom-size');
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main-menu' => esc_html__('Top menu', 'makacumbang'),
			)
		);
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'makacumbang_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'makacumbang_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function makacumbang_content_width()
{
	$GLOBALS['content_width'] = apply_filters('makacumbang_content_width', 640);
}
add_action('after_setup_theme', 'makacumbang_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function makacumbang_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'makacumbang'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'makacumbang'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'makacumbang_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function makacumbang_scripts()
{
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap-theme.min.css');
	wp_enqueue_style('fancybox', get_template_directory_uri() . '/assets/vendor/fancybox/dist/jquery.fancybox.min.css');
	wp_enqueue_style('slick', get_template_directory_uri() . '/assets/vendor/slick-carousel/slick/slick.css');
	wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/vendor/slick-carousel/slick/slick-theme.css');
	wp_enqueue_style('makacumbang-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('makacumbang-style', 'rtl', 'replace');
	wp_enqueue_script('core', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0', true);
	wp_enqueue_script('makacumbang-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js');
	// wp_enqueue_script('fancybox', get_template_directory_uri() . '/assets/vendor/fancybox/dist/jquery.fancybox.min.js');
	// wp_enqueue_script('gmap3', get_template_directory_uri() . '/assets/vendor/gmap3/dist/gmap3.min.js');
	wp_enqueue_script('isotope', get_template_directory_uri() . '/assets/vendor/isotope-layout/dist/isotope.pkgd.min.js');
	wp_enqueue_script('slick', get_template_directory_uri() . '/assets/vendor/slick-carousel/slick/slick.min.js');
	wp_enqueue_script('smooth-scroll', get_template_directory_uri() . '/assets/vendor/Sticky-Navigation-Smooth-Scroll-Scrollspy/dist/jquery.sticky-nav-1.1.0.min.js');

	/**
	 * Make admin URL available as JavaScript variable for portfolio ajax
	 */
	wp_enqueue_script('portfolio_ajax', get_template_directory_uri() . '/assets/ajax/portfolio.js', array('jquery'), '1.0', true);
	wp_enqueue_script('portfolio_archive_ajax', get_template_directory_uri() . '/assets/ajax/portfolio-archive.js', array('jquery'), '1.0', true);
	wp_enqueue_script('portfolio_archive_ajax_pagination', get_template_directory_uri() . '/assets/ajax/portfolio-pagination.js', array('jquery'), '1.0', true);
	wp_localize_script('portfolio_ajax', 'my_ajaxurl', admin_url('admin-ajax.php'));

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'makacumbang_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}


// Register Custom Post Type
function portofolio()
{

	$labels = array(
		'name'                  => _x('Portofolio', 'Post Type General Name', 'macankumbang'),
		'singular_name'         => _x('Portofolios', 'Post Type Singular Name', 'macankumbang'),
		'menu_name'             => __('Portofolio', 'macankumbang'),
		'name_admin_bar'        => __('Portofolio', 'macankumbang'),
		'archives'              => __('Item Archives', 'macankumbang'),
		'attributes'            => __('Item Attributes', 'macankumbang'),
		'parent_item_colon'     => __('Parent Item:', 'macankumbang'),
		'all_items'             => __('All Items', 'macankumbang'),
		'add_new_item'          => __('Add New Item', 'macankumbang'),
		'add_new'               => __('Add New', 'macankumbang'),
		'new_item'              => __('New Item', 'macankumbang'),
		'edit_item'             => __('Edit Item', 'macankumbang'),
		'update_item'           => __('Update Item', 'macankumbang'),
		'view_item'             => __('View Item', 'macankumbang'),
		'view_items'            => __('View Items', 'macankumbang'),
		'search_items'          => __('Search Item', 'macankumbang'),
		'not_found'             => __('Not found', 'macankumbang'),
		'not_found_in_trash'    => __('Not found in Trash', 'macankumbang'),
		'featured_image'        => __('Featured Image', 'macankumbang'),
		'set_featured_image'    => __('Set featured image', 'macankumbang'),
		'remove_featured_image' => __('Remove featured image', 'macankumbang'),
		'use_featured_image'    => __('Use as featured image', 'macankumbang'),
		'insert_into_item'      => __('Insert into item', 'macankumbang'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'macankumbang'),
		'items_list'            => __('Items list', 'macankumbang'),
		'items_list_navigation' => __('Items list navigation', 'macankumbang'),
		'filter_items_list'     => __('Filter items list', 'macankumbang'),
	);
	$args = array(
		'label'                 => __('Portofolios', 'macankumbang'),
		'description'           => __('Post Type Description', 'macankumbang'),
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', 'post-formats'),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('portofolio', $args);
}
add_action('init', 'portofolio', 0);

// Display menu
if (!function_exists('display_menu')) {
	function display_menu($slug)
	{
		$menu = array(
			'theme_location' => $slug,
			'container' => 'nav',
			'container_id' => 'myNavbar',
			'container_class' => 'navbar',
			'menu_class'        => 'active'
		);
		wp_nav_menu($menu);
	}
}

// Register Custom Taxonomy
function tutorial()
{

	$labels = array(
		'name'                       => _x('Tuts', 'Taxonomy General Name', 'macankumbang'),
		'singular_name'              => _x('Tut', 'Taxonomy Singular Name', 'macankumbang'),
		'menu_name'                  => __('Tut', 'macankumbang'),
		'all_items'                  => __('All Items', 'macankumbang'),
		'parent_item'                => __('Parent Item', 'macankumbang'),
		'parent_item_colon'          => __('Parent Item:', 'macankumbang'),
		'new_item_name'              => __('New Item Name', 'macankumbang'),
		'add_new_item'               => __('Add New Item', 'macankumbang'),
		'edit_item'                  => __('Edit Item', 'macankumbang'),
		'update_item'                => __('Update Item', 'macankumbang'),
		'view_item'                  => __('View Item', 'macankumbang'),
		'separate_items_with_commas' => __('Separate items with commas', 'macankumbang'),
		'add_or_remove_items'        => __('Add or remove items', 'macankumbang'),
		'choose_from_most_used'      => __('Choose from the most used', 'macankumbang'),
		'popular_items'              => __('Popular Items', 'macankumbang'),
		'search_items'               => __('Search Items', 'macankumbang'),
		'not_found'                  => __('Not Found', 'macankumbang'),
		'no_terms'                   => __('No items', 'macankumbang'),
		'items_list'                 => __('Items list', 'macankumbang'),
		'items_list_navigation'      => __('Items list navigation', 'macankumbang'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy('tut', array('portofolio'), $args);
}
add_action('init', 'tutorial', 0);

// Register Custom Post Type
require('create-custom-post-type.php');

// Create pagination
require('create-pagination.php');

//Get list post orderBy function
require('functions/Archive_Order_By_Date.php');





function get_categories_list()
{
	$term = get_the_terms(get_the_ID(), 'tut');
	$list = '';
	foreach ($term as $value) {
		$list .= $value->slug . ' ';
	}
	return $list;
}

/**
 * Functions ajax portfolio
 */
add_action('wp_ajax_browseall', 'browse_all_function');
add_action('wp_ajax_nopriv_browseall', 'browse_all_function');
function browse_all_function()
{
	$args = array(
		'post_type' => 'portofolio',
		'post_status' => 'publish',
		'posts_per_page' => $_GET['post_per_page'],
		'paged' => $_GET['currentPage'] + 1
	);
	$list_portofolio = new WP_Query($args);
	ob_start();
	if ($list_portofolio->have_posts()) {
		$next = 0;
		while ($list_portofolio->have_posts()) {
			$list_portofolio->the_post();
			get_template_part('template-parts/content', 'portfolio');
			$next++;
		}
		if ($next < $_GET['post_per_page']) $next = null;
	}
	$html = ob_get_contents();
	$response = array(
		'message' => 'success',
		'html' => $html,
		'next' => $next
	);
	ob_end_clean();
	wp_send_json_success($response, 200);
	wp_reset_postdata();
	die();
}


/**
 * Functions ajax archive-portfolio	
 */
require ('inc/archive-portfolio-ajax.php');

/**
 * Functions ajax pagination
 */
require ('assets/ajax/portfolio-pagination.php');

function custom_query_porfolio(WP_Query $query)
{

	if (is_post_type_archive('portofolio') && $query->is_main_query()) {
		$term_id = empty($_GET['term_id']) ? null : $_GET['term_id'];
		$order = empty($_GET['order']) ? null : $_GET['order'];
		if (!empty($term_id)) {
			$query->set('tax_query', array(                     //(array) - Lấy bài viết dựa theo taxonomy
				'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
				array(
					'taxonomy' => 'tut',                //(string) - Tên của taxonomy
					'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
					'terms' => array($term_id),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
					'include_children' => true,           //(bool) - Lấy category con, true hoặc false
					'operator' => 'IN'                    //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
				),
			));
		}
		$query->set('posts_per_page', 6);
		if (!empty($order)) {
			$query->set('orderby', 'date');
			$query->set('order', $order);
		}
	}

	return $query;
}
add_filter('pre_get_posts', 'custom_query_porfolio');
