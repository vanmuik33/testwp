<?php 
/* Thêm thông tin post type*/
function create_post_type($title,$name,$slug,$support,$hierachy="true"){
	$labels = array(
		'name' => $title,
		'singular_name' => $title,
		'add_new' => 'Add New',
		'add_new_item' => 'Add New',
		'edit_item' => 'Edit '.$title,
		'new_item' => 'New '.$title,
		'all_items' => 'All '.$title,
		'view_item' => 'View '.$title,
		'search_items' => 'Search '.$title,
		'not_found' => 'No post found',
		'not_found_in_trash' => 'No post found in Trash',
		'parent_item_colon' => '',
		'menu_name' => $title
	);
	$args = array(
		'labels' => $labels,
		'menu_icon' => null,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'hierarchical' => $hierachy,
		'menu_position' => 5,
		'rewrite' => array( 'slug' =>$slug),
		'supports' => $support
	);
	register_post_type($name, $args );
}


/* Thêm thông tin taxonomy  */
function create_taxonomy_theme($title="Category",$slug,$name,$post_type) {
	$labels = array(
		'name' => $title,
		'singular' => $title,
		'menu_name' => $title
	);
	$args = array(
		'labels' => $labels,
		'show_admin_column' => true,
		'hierarchical' => true,
		'public' => true,
		'rewrite' => array('slug' => $slug),
		'show_tagcloud' => true
	);
	register_taxonomy($name,$post_type,$args);
}