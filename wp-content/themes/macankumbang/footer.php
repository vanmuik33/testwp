<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makacumbang
 */

?>

<section id="footer">
	<div class="container">
		<p class="cpright">(c) copyright 2013 macankumbang </p>
		<a href="#"><img class="hori-center" src="<?php echo TPL_DIR_URI ?>/assets/images/header/logo.png" alt="Macankumbang"></a>
	</div>
</section>
</div><!-- #page -->

<?php wp_footer(); ?>



</body>

</html>