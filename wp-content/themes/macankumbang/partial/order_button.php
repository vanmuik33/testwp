<?php
    get_template_part('functions/get_current_url');
    if (isset($_GET['term_id'])) {
        $term_id = 'term_id='.$_GET['term_id'];
    }
    else {
        $term_id ='';
    }
?>
<div class="btn-group">
    <button type="button" class="btn btn-info">Sắp xếp</button>
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu">
        <li id="desc" class="dropdown-item" data-order="desc" href="<?php $currentUrl;echo '?'.$term_id.'&' ?>orderby=date&order=desc">Mới nhất</li>
        <li id="asc" class="dropdown-item" data-order="asc" href="<?php $currentUrl;echo '?'.$term_id.'&' ?>orderby=date&order=asc">Cũ nhất</li>
        <!-- <li><a id="desc" class="dropdown-item" data-order="desc" href="<?php $currentUrl;echo '?'.$term_id.'&' ?>orderby=date&order=desc">Mới nhất</a></li>
        <li><a id="asc" class="dropdown-item" data-order="asc" href="<?php $currentUrl;echo '?'.$term_id.'&' ?>orderby=date&order=asc">Cũ nhất</a></li> -->
    </ul>
</div>