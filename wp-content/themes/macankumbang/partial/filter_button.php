<?php
get_template_part('functions/get_current_url')
?>

<ol class="breadcrumb hori-center">
    <?php
    $args = array(
        // 'type'                     => 'portofolio',
        'taxonomy'                 => 'tut',
        'hide_empty' => false
    );
    if (isset($_GET['orderby'])) {
        $orderBy = 'orderby=' . $_GET['orderby'];
        $order = 'order=' . $_GET['order'];
    } else {
        $orderBy = '';
        $order = '';
    }
    ?>
    <li class="active filter-button" data-filter="" >All</li>
    <!-- <li class="active" ><a href="<?php $currentUrl;
                                echo '?' . $order . '&' . $orderBy ?>">All</a></li> -->
    <?php
    $taxonomies = get_terms($args);
    foreach ($taxonomies as $taxonomy) {
    ?>
        <li class="filter-button" data-filter="<?php echo $taxonomy->slug ?>"><?php echo $taxonomy->name; ?></li>
        <!-- <li><a href="<?php echo get_post_type_archive_link('portofolio');
                        echo '?' . $order . '&' . $orderBy . '&' ?>term_id=<?php echo $taxonomy->term_id ?>"><?php echo $taxonomy->name; ?></a></li> -->
    <?php
    }
    ?>
</ol>