<?php

/**
 * The template for displaying archive portfolio pages
 *
 * @package Makacumbang
 */
get_header();
?>

<main id="primary" class="site-main">
	<?php if (have_posts()) : ?>
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<?php get_template_part('partial/order_button') ?>
				</div>
				<div class="col-sm-4">
					<?php get_template_part('partial/filter_button') ?>
				</div>
			</div>
			<input type="hidden" id="last_order" value="">
			<input type="hidden" id="last_filter" value="">
			<input type="hidden" id="paged" value="">
			<hr>
			<div class="row" id="archive-content">
				<?php while (have_posts()) : the_post(); ?>
					<!-- <div class="col-sm-4"> -->
					<?php get_template_part('template-parts/content', 'portfolio-archive') ?>
					<!-- </div> -->
				<?php
				endwhile; ?>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<!-- <?php
							global $wp_query;
							html5wp_pagination($wp_query); ?> -->

					<div id="pagination">
						<ul class="pagination bounceInUp animated wow" data-wow-delay=".8s">
							<?php global $wp_query;
								$total_page = $wp_query->max_num_pages;
								$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
								if ($total_page > 1) {
									for ($i = 1 ; $i <= $total_page ; $i++) {
										if ($i == $current_page) {
											echo '<li><span aria-current="page" class="page-numbers current">' . $current_page . '</span></li>';
										} else {
											echo '<li><a class="page-numbers" data-page="'.$i.'" href="#">' . $i . '</a></li>';
										}
									}
								}
								
							?>
							<!-- <li><span aria-current="page" class="page-numbers current">1</span></li>
							<li><a class="page-numbers" href="#">2</a></li>
							<li><a class="next page-numbers" href="#">Next »</a></li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>

	<?php
	else :
		// get_template_part('template-parts/content', 'none');
		echo 'not found';
	endif;
	?>
</main><!-- #main -->

<?php
get_footer();
